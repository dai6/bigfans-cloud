package com.bigfans.paymentservice;

import com.bigfans.framework.Applications;
import com.bigfans.framework.CurrentUser;

/**
 * @author lichong
 * @create 2018-04-27 下午9:00
 **/
public class PaymentApplications extends Applications {

    private static String functionalUserToken;
    private static CurrentUser functionalUser;

    public static void setFunctionalUser(CurrentUser functionalUser) {
        PaymentApplications.functionalUser = functionalUser;
    }

    public static CurrentUser getFunctionalUser() {
        return functionalUser;
    }

    public static void setFunctionalUserToken(String functionalUserToken) {
        PaymentApplications.functionalUserToken = functionalUserToken;
    }

    public static String getFunctionalUserToken() {
        return functionalUserToken;
    }
}
