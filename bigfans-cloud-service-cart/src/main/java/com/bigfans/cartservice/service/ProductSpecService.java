package com.bigfans.cartservice.service;

import com.bigfans.cartservice.model.ProductSpec;
import com.bigfans.framework.dao.BaseService;

import java.util.List;

public interface ProductSpecService extends BaseService<ProductSpec>{

    List<ProductSpec> listProductSpecs(String prodId) throws Exception;

}
