package com.bigfans.cartservice.exception;

import com.bigfans.framework.exception.ServiceRuntimeException;

/**
 * 
 * @Description:库存不足
 * @author lichong
 * 2015年8月28日上午11:28:25
 *
 */
public class StockInsufficientException extends ServiceRuntimeException {

	private static final long serialVersionUID = -2954947819671607683L;

	public StockInsufficientException() {
	}
	
	public StockInsufficientException(String message) {
		super(message);
	}
}
