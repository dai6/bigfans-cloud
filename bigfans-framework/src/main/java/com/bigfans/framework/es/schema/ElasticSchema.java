package com.bigfans.framework.es.schema;

import org.elasticsearch.common.settings.Settings.Builder;

public interface ElasticSchema {

	void build(Builder settingsBuilder);
	
}
