package com.bigfans.searchservice.model;

import com.bigfans.framework.model.AbstractModel;
import lombok.Data;

import java.util.Date;

@Data
public class Category extends AbstractModel {
	
	private static final long serialVersionUID = -8722369963771498740L;

	protected String name;
	protected String parentId;
	protected String imagePath;
	protected Integer level;
	protected Integer orderNum;
	protected Boolean showInNav;
	protected Boolean showInHome;
	protected String description;

	@Override
	public String getModule() {
		return null;
	}
}
