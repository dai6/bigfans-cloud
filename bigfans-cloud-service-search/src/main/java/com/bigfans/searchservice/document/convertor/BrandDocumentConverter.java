package com.bigfans.searchservice.document.convertor;

import com.bigfans.framework.es.DocumentConverter;
import com.bigfans.framework.es.IndexDocument;
import com.bigfans.searchservice.model.Brand;
import com.bigfans.searchservice.schema.mapping.BrandMapping;

import java.util.Map;

/**
 * 
 * @Description:
 * @author lichong
 * 2015年5月8日上午9:40:58
 *
 */
public class BrandDocumentConverter implements DocumentConverter<Brand> {

	@Override
	public Brand toObject(Map<String, Object> dataMap) {
		Brand brand = new Brand();
		brand.setId((String)dataMap.get(BrandMapping.FIELD_ID));
		brand.setName((String)dataMap.get(BrandMapping.FIELD_NAME));
		brand.setDescription((String)dataMap.get(BrandMapping.FIELD_DESC));
		brand.setLogo((String)dataMap.get(BrandMapping.FIELD_LOGO));
		brand.setRecommended("1".equals(dataMap.get(BrandMapping.FIELD_REC)));
		brand.setCategoryId((String)dataMap.get(BrandMapping.FIELD_CATEGORY_ID));
		return brand;
	}

	@Override
	public IndexDocument toDocument(Brand obj) {
		IndexDocument doc = new IndexDocument(obj.getId());
		doc.put(BrandMapping.FIELD_ID, obj.getId());
		doc.put(BrandMapping.FIELD_NAME, obj.getName());
		doc.put(BrandMapping.FIELD_DESC, obj.getDescription());
		doc.put(BrandMapping.FIELD_LOGO, obj.getLogo());
		doc.put(BrandMapping.FIELD_CATEGORY_ID, obj.getCategoryId());
		doc.put(BrandMapping.FIELD_REC, obj.getRecommended() ? 1 : 0);
		return doc;
	}

}
