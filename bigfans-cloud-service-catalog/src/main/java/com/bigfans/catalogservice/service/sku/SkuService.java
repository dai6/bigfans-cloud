package com.bigfans.catalogservice.service.sku;

import com.bigfans.catalogservice.model.SKU;
import com.bigfans.framework.dao.BaseService;

import java.util.List;

/**
 * 
 * @Description:SKU
 * @author lichong 2015年5月30日下午10:56:34
 *
 */
public interface SkuService extends BaseService<SKU> {

	SKU getByPid(String pid) throws Exception;
	
	SKU getByValKey(String valKey) throws Exception;
	
	List<SKU> listByPgId(String pgId) throws Exception;

	List<SKU> listByProdId(String prodId) throws Exception;
	
}
