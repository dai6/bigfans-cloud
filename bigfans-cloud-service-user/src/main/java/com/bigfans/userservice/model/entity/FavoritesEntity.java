package com.bigfans.userservice.model.entity;

import com.bigfans.framework.model.AbstractModel;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;

/**
 * 
 * @Description: 用户收藏
 * @author lichong
 * @date Mar 10, 2016 3:20:44 PM 
 *
 */
@Data
@Table(name="Favorites")
public class FavoritesEntity extends AbstractModel {
	
	private static final long serialVersionUID = 2974482257273664264L;
	
	@Column(name="user_id")
	protected String userId;
	@Column(name="prod_id")
	protected String productId;

	@Override
	public String getModule() {
		return "Favorite";
	}
}
