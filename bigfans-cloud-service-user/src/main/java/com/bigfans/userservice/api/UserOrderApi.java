package com.bigfans.userservice.api;

import com.bigfans.framework.Applications;
import com.bigfans.framework.CurrentUser;
import com.bigfans.framework.annotations.NeedLogin;
import com.bigfans.framework.web.BaseController;
import com.bigfans.framework.web.RestResponse;
import com.bigfans.userservice.api.form.UsePropertyForm;
import com.bigfans.userservice.exception.UsePropertyFailureException;
import com.bigfans.userservice.service.UserPropertyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserOrderApi extends BaseController {

    @Autowired
    private UserPropertyService userPropertyService;

    @PostMapping(value = "/useProperty")
    @NeedLogin
    public RestResponse useProperty(@RequestBody UsePropertyForm form) throws Exception {
        CurrentUser currentUser = Applications.getCurrentUser();
        try{
            userPropertyService.useProperty(currentUser.getUid(), form.getOrderId(), form.getCouponId(), form.getPoints(), form.getBalance());
        }catch (Exception e){
            throw new UsePropertyFailureException();
        }
        return RestResponse.ok();
    }

}
