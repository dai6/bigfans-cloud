package com.bigfans.userservice.api.auth;

import com.bigfans.framework.CurrentUser;

import java.util.Arrays;
import java.util.Date;

/**
 * @author lichong
 * @create 2018-04-27 下午8:47
 **/
public class UserServiceFunctionalUser extends CurrentUser {

    public UserServiceFunctionalUser() {
        this.setAccount("user-service-functional-id");
        this.setLoggedIn(true);
        this.setType(TYPE.FUNCTIONAL_ID);
        this.setPeriod(-1);
        this.setLoginTime(new Date());
        this.setPermissions(Arrays.asList("all"));
        this.setRoles(Arrays.asList("admin"));
    }

}
